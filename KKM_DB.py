#! /usr/bin/python
import re

SOURCE = 'KKM_DB.org'
SOURCE2 = 'etc/RepertoireDB.org'

rx_section = re.compile(r'''
    ^
    \*{2}\s+(?P<title>.+?)\s+:parse:\s*[\r\n]
    (?P<content>[\s\S]+?)
    (?=^\*{2}\s|\Z)
''', re.MULTILINE | re.VERBOSE)

rx_performance = re.compile(r'''
    ^
    \*{3}\s+\[(?P<date>[-0-9]+)\](?P<title>.+?)\s*[\r\n]
    (?P<content>[\s\S]+?)
    (?=^\*{3}\s|\Z)
''', re.MULTILINE | re.VERBOSE)

rx_ptitle = re.compile(r'''
    ^
    \*+\s+(?P<composer>.*?)\s+--\s+(?P<title>.+?)\s*[\r\n]
    \s*:PROPERTIES:\s*[\r\n]
#    \s*:ID:\s+(?P<ID>[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12})\s*[\r\n]
    \s*:ID:\s+(?P<ID>[-0-9a-z]+)\s*[\r\n]
    \s*:END:
''', re.MULTILINE | re.VERBOSE) 

rx_title = re.compile(r'''
    ^
    \*+\s+(?P<composer>.*?)\s+--\s+(?P<title>.+?)\s*[\r\n]
    \s*:PROPERTIES:\s*[\r\n]
    \s*(?P<properties>[\s\S]+?)
    \s*:END:
''', re.MULTILINE | re.VERBOSE) 

rx_properties = re.compile(r'''
    ^
    \s*:(?P<key>\w+):\s+(?P<value>[\S\s]+?)
    \s*([\r\n] | \Z)
''', re.MULTILINE | re.VERBOSE)


def exportTex():
    for title in props:
        print('\\begin{title}')
        for k in title.keys():
            # print('{}: {}'.format(k, title[k]))
            print('  \\{}{{{}}}'.format(k, title[k]))
            for g in gigs:
                if g[1] == title['ID']:
                    print('  \\{}{{{}}}'.format('Performed', g[0]))
            print('\\end{title}\n')

def exportCSV():
    CSVcolumns = ['key', 'title', 'collection', 'composer', 'composerID']
    for title in props:
        key = title.get('Wikidata')
        title = title.get('Title')
        collection = title.get('Collection')
        composer = title.get('Composer')
        composerID = title.get('ComposerID')



with open(SOURCE, "r") as source:
    content = source.read()
    sections = {section.group('title') : section.group('content')
                for section in rx_section.finditer(content)
    }

    # read title IDs for each performance
    gigs = ((gig.group('date'), title.group('ID'))
               for gig in rx_performance.finditer(sections['Performances'])
               for title in rx_ptitle.finditer(gig.group('content'))
    )

    titles = (title.groupdict()
              for title in rx_title.finditer(sections['Titles'])
    )

    props = ({p[0] : p[1] for p in rx_properties.findall(t['properties'])}
              for t in titles
    )



    for title in gigs:
        print(title)
        
