\babel@toc {ngerman}{}
\contentsline {section}{Auftritte}{15}{section*.1}
\contentsline {subsection}{2018-12-24\hskip 1em\relax Christvesper}{15}{subsection*.2}
\contentsline {subsection}{2018-12-16\hskip 1em\relax W. A. Mozart: c-Moll Messe}{15}{subsection*.3}
\contentsline {subsection}{2018-11-24\hskip 1em\relax Taufe von Johanna und Enno}{15}{subsection*.4}
\contentsline {subsection}{2018-10-31\hskip 1em\relax Reformationsfeier}{15}{subsection*.5}
\contentsline {subsection}{2018-10-07\hskip 1em\relax Gottesdienst zum 19. Sonntag nach Trinitatis}{15}{subsection*.6}
\contentsline {subsection}{2018-06-16\hskip 1em\relax geerdet, Chor- und Orgelmusik}{15}{subsection*.7}
\contentsline {subsection}{2018-04-30\hskip 1em\relax Konzert im Temple Neuf, Metz}{16}{subsection*.8}
\contentsline {subsection}{2018-04-29\hskip 1em\relax Gottesdienst im Temple protestant de Nancy}{16}{subsection*.9}
\contentsline {subsection}{2018-04-01\hskip 1em\relax Konzert in der Cath\IeC {\'e}drale Notre-Dame-de-l'Annonciation de Nancy}{16}{subsection*.10}
\contentsline {subsection}{2018-03-30\hskip 1em\relax Gottesdienst zum Karfreitag}{16}{subsection*.11}
\contentsline {subsection}{2018-01-06\hskip 1em\relax Er\IeC {\"o}ffnung der 21. Mannheimer Vesperkirche}{16}{subsection*.12}
\contentsline {subsection}{2017-12-24\hskip 1em\relax Christvesper}{16}{subsection*.13}
\contentsline {subsection}{2017-12-10\hskip 1em\relax J. S. Bach: h-Moll Messe}{16}{subsection*.14}
\contentsline {subsection}{2017-12-03\hskip 1em\relax Gottesdienst zum 1. Advent}{16}{subsection*.15}
\contentsline {subsection}{2017-10-31\hskip 1em\relax Reformationsfeier}{16}{subsection*.16}
\contentsline {subsection}{2017-09-24\hskip 1em\relax Gottesdienst zum 15. Sonntag nach Trinitatis}{17}{subsection*.17}
\contentsline {subsection}{2017-07-08\hskip 1em\relax \IeC {\"O}kumenischer Kirchentag}{17}{subsection*.18}
\contentsline {subsection}{2017-07-01\hskip 1em\relax Chorfest Heidelberg}{17}{subsection*.19}
\contentsline {subsection}{2017-04-14\hskip 1em\relax Gottesdienst zum Karfreitag}{17}{subsection*.20}
\contentsline {subsection}{2017-01-06\hskip 1em\relax Er\IeC {\"o}ffnung der 20. Mannheimer Vesperkirche}{17}{subsection*.21}
\contentsline {subsection}{2016-12-04\hskip 1em\relax J. S. Bach: Gloriakantate, Weihnachtsoratorium}{18}{subsection*.22}
\contentsline {subsection}{2016-10-31\hskip 1em\relax Reformationsfeier}{18}{subsection*.23}
\contentsline {subsection}{2016-10-16\hskip 1em\relax Taufe von Yara}{18}{subsection*.24}
\contentsline {subsection}{2016-09-18\hskip 1em\relax Gottesdienst zum 17. Sonntag nach Trinitatis}{18}{subsection*.25}
\contentsline {subsection}{2016-06-12\hskip 1em\relax Wenn Engel reisen}{18}{subsection*.26}
\contentsline {subsection}{2016-06-12\hskip 1em\relax Gottesdienst zum 3. Sonntag nach Trinitatis}{19}{subsection*.27}
\contentsline {subsection}{2016-05-07\hskip 1em\relax Wenn Engel reisen}{19}{subsection*.28}
\contentsline {subsection}{2016-05-06\hskip 1em\relax Wenn Engel reisen}{19}{subsection*.29}
\contentsline {subsection}{2016-03-25\hskip 1em\relax Fernsehgottesdienst zum Ostersonntag}{19}{subsection*.30}
\contentsline {subsection}{2016-01-06\hskip 1em\relax Er\IeC {\"o}ffnung der 19. Mannheimer Vesperkirche}{19}{subsection*.31}
\contentsline {subsection}{2015-12-24\hskip 1em\relax Christvesper}{19}{subsection*.32}
\contentsline {subsection}{2015-12-23\hskip 1em\relax Weihnachtsliedersingen f\IeC {\"u}r die Fl\IeC {\"u}chtlinge}{19}{subsection*.33}
\contentsline {subsection}{2015-11-29\hskip 1em\relax Gottesdienst zum 1. Advent}{19}{subsection*.34}
\contentsline {subsection}{2015-11-22\hskip 1em\relax J. C. Bach, Requiem}{19}{subsection*.35}
\contentsline {subsection}{2015-10-31\hskip 1em\relax Reformationsfeier}{20}{subsection*.36}
\contentsline {subsection}{2015-07-11\hskip 1em\relax Sommer Nacht Musik}{20}{subsection*.37}
\contentsline {subsection}{2015-06-28\hskip 1em\relax FriedensSeufzer}{20}{subsection*.38}
\contentsline {subsection}{2015-04-03\hskip 1em\relax Gottesdienst zum Karfreitag}{20}{subsection*.39}
\contentsline {subsection}{2015-02-08\hskip 1em\relax Verabschiedung von Peter Anweiler}{20}{subsection*.40}
\contentsline {subsection}{2015-01-06\hskip 1em\relax Er\IeC {\"o}ffnung der 18. Mannheimer Vesperkirche}{20}{subsection*.41}
\contentsline {subsection}{2014-12-24\hskip 1em\relax Christvesper}{20}{subsection*.42}
\contentsline {subsection}{2014-12-22\hskip 1em\relax Singen in der LEA}{20}{subsection*.43}
\contentsline {subsection}{2014-12-07\hskip 1em\relax L. Bernstein, Chichester Psalms}{21}{subsection*.44}
\contentsline {subsection}{2014-11-30\hskip 1em\relax Gottesdienst zum 1. Advent}{21}{subsection*.45}
\contentsline {subsection}{2014-10-31\hskip 1em\relax Reformationsfeier}{21}{subsection*.46}
\contentsline {subsection}{2014-09-21\hskip 1em\relax Gottesdienst zum 14. Sonntag nach Trinitatis}{21}{subsection*.47}
\contentsline {subsection}{2014-07-06\hskip 1em\relax Visitationsgottesdienst zum 3. Sonntag nach Trinitatis}{21}{subsection*.48}
\contentsline {subsection}{2014-05-18\hskip 1em\relax A cappella meets Jazz}{21}{subsection*.49}
\contentsline {subsection}{2014-05-04\hskip 1em\relax Gottesdienst in Saint-Louis en l'Ille}{22}{subsection*.50}
\contentsline {subsection}{2014-05-03\hskip 1em\relax Concert de musique sacre\IeC {\'e} a cappella in Paris}{22}{subsection*.51}
\contentsline {subsection}{2014-05-02\hskip 1em\relax Concert de musique sacre\IeC {\'e} a cappella in Versailles}{22}{subsection*.52}
\contentsline {subsection}{2014-04-18\hskip 1em\relax Gottesdienst zum Karfreitag}{22}{subsection*.53}
\contentsline {subsection}{2014-01-06\hskip 1em\relax Er\IeC {\"o}ffnung der 17. Mannheimer Vesperkirche}{22}{subsection*.54}
\contentsline {subsection}{2013-12-24\hskip 1em\relax Christvesper}{22}{subsection*.55}
\contentsline {subsection}{2013-11-24\hskip 1em\relax Aus der Tiefe rufe ich, Herr, zu Dir}{22}{subsection*.56}
\contentsline {subsection}{2013-10-31\hskip 1em\relax Reformationsfeier}{22}{subsection*.57}
\contentsline {subsection}{2013-03-28\hskip 1em\relax Gottesdienst zum Karfreitag}{22}{subsection*.58}
\contentsline {subsection}{2013-03-17\hskip 1em\relax J. S. Bach: Johannespassion}{22}{subsection*.59}
\contentsline {subsection}{2013-01-06\hskip 1em\relax Er\IeC {\"o}ffnung der 16. Mannheimer Vesperkirche}{23}{subsection*.60}
\contentsline {subsection}{2012-12-24\hskip 1em\relax Christvesper}{23}{subsection*.61}
\contentsline {subsection}{2012-12-02\hskip 1em\relax J. S. Bach: Weihnachtsoratorium 1\IeC {\textendash }3}{23}{subsection*.62}
\contentsline {subsection}{2012-12-02\hskip 1em\relax Weihnachtsoratorium f\IeC {\"u}r Kinder}{23}{subsection*.63}
\contentsline {subsection}{2012-10-31\hskip 1em\relax Reformationsfeier}{23}{subsection*.64}
\contentsline {subsection}{2012-05-18\hskip 1em\relax Nachtkonzert}{23}{subsection*.65}
\contentsline {subsection}{2012-05-13\hskip 1em\relax Lobet den Herrn!}{23}{subsection*.66}
\contentsline {subsection}{2012-05-01\hskip 1em\relax Lobet den Herrn, Werken voor koor en orgel}{24}{subsection*.67}
\contentsline {subsection}{2012-04-28\hskip 1em\relax Lobet den Herrn, Werken voor koor en orgel}{24}{subsection*.68}
\contentsline {subsection}{2012-03-22\hskip 1em\relax Einf\IeC {\"u}hrung Dekan Hartmann}{24}{subsection*.69}
\contentsline {subsection}{2012-04-06\hskip 1em\relax Gottesdienst zum Karfreitag}{24}{subsection*.70}
\contentsline {subsection}{2012-03-25\hskip 1em\relax Gottesdienst zur Verabschiedung von Dekan Eitenm\IeC {\"u}ller}{24}{subsection*.71}
\contentsline {subsection}{2012-01-06\hskip 1em\relax Er\IeC {\"o}ffnung der 15. Mannheimer Vesperkirche}{24}{subsection*.72}
\contentsline {subsection}{2011-12-24\hskip 1em\relax Christvesper}{24}{subsection*.73}
\contentsline {subsection}{2011-11-20\hskip 1em\relax J. Brahms: Ein deutsches Requiem}{24}{subsection*.74}
\contentsline {subsection}{2011-10-31\hskip 1em\relax Reformationsfeier}{24}{subsection*.75}
\contentsline {subsection}{2011-09-11\hskip 1em\relax Gottesdienst zum 12. Sonntag nach Trinitatis}{25}{subsection*.76}
\contentsline {subsection}{2011-07-09\hskip 1em\relax 100 Jahre Christuskirche}{25}{subsection*.77}
\contentsline {subsection}{2011-05-11\hskip 1em\relax Max Reger, Anton Bruckner}{25}{subsection*.78}
\contentsline {subsection}{2011-04-22\hskip 1em\relax Gottesdienst zum Karfreitag}{25}{subsection*.79}
\contentsline {subsection}{2011-01-06\hskip 1em\relax Er\IeC {\"o}ffnung der 14. Mannheimer Vesperkirche}{25}{subsection*.80}
\contentsline {subsection}{2010-12-24\hskip 1em\relax Christvesper}{25}{subsection*.81}
\contentsline {subsection}{2010-12-05\hskip 1em\relax G. Puccini: Missa di Gloria}{25}{subsection*.82}
\contentsline {subsection}{2010-11-28\hskip 1em\relax Gottesdienst zum 1. Advent}{25}{subsection*.83}
\contentsline {subsection}{2010-10-31\hskip 1em\relax Reformationsfeier}{26}{subsection*.84}
\contentsline {subsection}{2010-10-30\hskip 1em\relax Geburtstag Herr Klein}{26}{subsection*.85}
\contentsline {subsection}{2010-10-10\hskip 1em\relax Gottesdienst zur Amtseinf\IeC {\"u}hrung von Anne Ressel}{26}{subsection*.86}
\contentsline {subsection}{2010-09-19\hskip 1em\relax Gottesdienst zum 16. Sonntag nach Trinitatis}{26}{subsection*.87}
\contentsline {subsection}{2010-06-13\hskip 1em\relax Sweeter than roses}{26}{subsection*.88}
\contentsline {subsection}{2010-05-13\hskip 1em\relax Sumer is icumen in}{26}{subsection*.89}
\contentsline {subsection}{2010-04-04\hskip 1em\relax Rundfunkgottesdienst zum Ostersonntag}{27}{subsection*.90}
\contentsline {subsection}{2010-01-06\hskip 1em\relax Er\IeC {\"o}ffnung der 13. Mannheimer Vesperkirche}{27}{subsection*.91}
\contentsline {subsection}{2009-12-24\hskip 1em\relax Christvesper}{27}{subsection*.92}
\contentsline {subsection}{2009-12-13\hskip 1em\relax C. Monteverdi, Marienvesper}{27}{subsection*.93}
\contentsline {subsection}{2009-10-31\hskip 1em\relax Reformationsfeier}{27}{subsection*.94}
\contentsline {subsection}{2009-06-28\hskip 1em\relax Romantische Chor- und Orgelmusik}{28}{subsection*.95}
\contentsline {subsection}{2009-04-10\hskip 1em\relax Gottesdienst zum Karfreitag}{28}{subsection*.96}
\contentsline {subsection}{2009-01-06\hskip 1em\relax Er\IeC {\"o}ffnung der 12. Mannheimer Vesperkirche}{28}{subsection*.97}
\contentsline {subsection}{2008-12-24\hskip 1em\relax Christvesper}{28}{subsection*.98}
\contentsline {subsection}{2008-11-23\hskip 1em\relax Mozart-Requiem}{28}{subsection*.99}
\contentsline {subsection}{2008-10-31\hskip 1em\relax Reformationsfeier}{28}{subsection*.100}
\contentsline {subsection}{2008-06-15\hskip 1em\relax Jubilalalalate Deo}{28}{subsection*.101}
\contentsline {subsection}{2008-05-03\hskip 1em\relax Chormusik a cappella}{29}{subsection*.102}
\contentsline {subsection}{2008-05-01\hskip 1em\relax Chormusik a cappella}{29}{subsection*.103}
\contentsline {subsection}{2008-03-31\hskip 1em\relax Gottesdienst zum Karfreitag}{29}{subsection*.104}
\contentsline {subsection}{2008-01-06\hskip 1em\relax Er\IeC {\"o}ffnung der 11. Mannheimer Vesperkirche}{29}{subsection*.105}
\contentsline {subsection}{2007-12-24\hskip 1em\relax Christvesper}{29}{subsection*.106}
\contentsline {subsection}{2007-12-02\hskip 1em\relax J. S. Bach: Weihnachtsoratorium 4\IeC {\textendash }6}{29}{subsection*.107}
\contentsline {subsection}{2007-10-31\hskip 1em\relax Reformationsfeier}{29}{subsection*.108}
\contentsline {subsection}{2007-06-17\hskip 1em\relax Konkordienkantorei\IeC {${}^2$} \IeC {\textendash } Eine Liebeserkl\IeC {\"a}rung an Mannheim}{29}{subsection*.109}
\contentsline {subsection}{2007-04-06\hskip 1em\relax Gottesdienst zum Karfreitag}{30}{subsection*.110}
\contentsline {subsection}{2007-01-06\hskip 1em\relax Er\IeC {\"o}ffnung der 10. Mannheimer Vesperkirche}{30}{subsection*.111}
\contentsline {subsection}{2006-12-24\hskip 1em\relax Christvesper}{30}{subsection*.112}
\contentsline {subsection}{2006-12-10\hskip 1em\relax J. S. Bach: Weihnachtsoratorium 1\IeC {\textendash }3}{30}{subsection*.113}
\contentsline {subsection}{2006-10-31\hskip 1em\relax Reformationsfeier}{30}{subsection*.114}
\contentsline {subsection}{2006-10-15\hskip 1em\relax \IeC {\quotedblbase }... wie Bach in Frankreich\IeC {\textquotedblright }}{30}{subsection*.115}
\contentsline {section}{Werke}{31}{section*.116}
\contentsline {subsection}{A hymn to the Virgin}{31}{subsection*.117}
\contentsline {subsection}{Abendlied}{31}{subsection*.118}
\contentsline {subsection}{Abendsegen}{32}{subsection*.119}
\contentsline {subsection}{Ach Herr, la\IeC {\ss } dein lieb Engelein}{32}{subsection*.120}
\contentsline {subsection}{Agnus Dei}{33}{subsection*.121}
\contentsline {subsection}{Agnus Dei}{33}{subsection*.122}
\contentsline {subsection}{Alle Welt springe}{34}{subsection*.123}
\contentsline {subsection}{Aller Augen warten auf dich, Herr}{34}{subsection*.124}
\contentsline {subsection}{Aller Augen warten auf dich, Herr}{35}{subsection*.125}
\contentsline {subsection}{Alta trinita beata}{35}{subsection*.126}
\contentsline {subsection}{Aus der Tiefe}{36}{subsection*.127}
\contentsline {subsection}{Aus der Tiefe}{36}{subsection*.128}
\contentsline {subsection}{Aus der Tiefe}{37}{subsection*.129}
\contentsline {subsection}{Aus der Tiefen rufe ich Herr zu dir}{37}{subsection*.130}
\contentsline {subsection}{Aus tiefer Not}{38}{subsection*.131}
\contentsline {subsection}{Aus tiefer Not}{38}{subsection*.132}
\contentsline {subsection}{Aus tiefer Not schrei ich zu Dir}{39}{subsection*.133}
\contentsline {subsection}{Ave Maria}{39}{subsection*.134}
\contentsline {subsection}{Ave Maria (Angelus Domini)}{40}{subsection*.135}
\contentsline {subsection}{Ave, maris stella}{40}{subsection*.136}
\contentsline {subsection}{Bachs Weihnachtsoratorium f\IeC {\"u}r Kinder}{41}{subsection*.137}
\contentsline {subsection}{Bleibet hier}{41}{subsection*.138}
\contentsline {subsection}{Cantate Domino}{42}{subsection*.139}
\contentsline {subsection}{Cantate Domino}{42}{subsection*.140}
\contentsline {subsection}{Cantate Domino}{43}{subsection*.141}
\contentsline {subsection}{Cantate Domino}{43}{subsection*.142}
\contentsline {subsection}{Chichester Psalms}{44}{subsection*.143}
\contentsline {subsection}{Credo}{44}{subsection*.144}
\contentsline {subsection}{Da unten im Tale}{45}{subsection*.145}
\contentsline {subsection}{Das Sursum Corda}{45}{subsection*.146}
\contentsline {subsection}{Denn Er hat seinen Engeln befohlen}{46}{subsection*.147}
\contentsline {subsection}{Deo dicamus gratias}{46}{subsection*.148}
\contentsline {subsection}{Der 100. Psalm -- Jauchzet dem Herrn, alle Welt}{47}{subsection*.149}
\contentsline {subsection}{Der 130. Psalm -- Aus der Tiefe ruf ich, Herr zu dir}{47}{subsection*.150}
\contentsline {subsection}{Der 2. Psalm -- Warum toben die Heiden}{48}{subsection*.151}
\contentsline {subsection}{Der 2. Psalm: Warum toben die Heiden}{48}{subsection*.152}
\contentsline {subsection}{Der 43. Psalm: Richte mich, Gott}{49}{subsection*.153}
\contentsline {subsection}{Der Mensch lebt und bestehet}{49}{subsection*.154}
\contentsline {subsection}{Der Mond ist aufgegangen}{50}{subsection*.155}
\contentsline {subsection}{Der Morgenstern ist aufgedrungen}{50}{subsection*.156}
\contentsline {subsection}{Det \IeC {\"a}r en ros utsprungen}{51}{subsection*.157}
\contentsline {subsection}{Die K\IeC {\"o}nige}{51}{subsection*.158}
\contentsline {subsection}{Die K\IeC {\"o}nige}{52}{subsection*.159}
\contentsline {subsection}{Dixit Dominus}{52}{subsection*.160}
\contentsline {subsection}{Dixit Maria ad Angelum}{53}{subsection*.161}
\contentsline {subsection}{Domine ad adiuvandum}{53}{subsection*.162}
\contentsline {subsection}{Durch dein Gef\IeC {\"a}ngnis}{54}{subsection*.163}
\contentsline {subsection}{Ein feste Burg ist unser Gott}{54}{subsection*.164}
\contentsline {subsection}{En natus est Emanuel}{55}{subsection*.165}
\contentsline {subsection}{Er nahm alles Wohl in acht}{55}{subsection*.166}
\contentsline {subsection}{Es ist ein Ros entsprungen}{56}{subsection*.167}
\contentsline {subsection}{Freut euch, wir sind Gottes Volk}{56}{subsection*.168}
\contentsline {subsection}{F\IeC {\"u}rchte dich nicht, ich bin bei dir}{57}{subsection*.169}
\contentsline {subsection}{Geht hin, ihr gl\IeC {\"a}ubigen Gedanken}{57}{subsection*.170}
\contentsline {subsection}{Gloria}{58}{subsection*.171}
\contentsline {subsection}{Gloria}{58}{subsection*.172}
\contentsline {subsection}{Gloria in excelsis Deo}{59}{subsection*.173}
\contentsline {subsection}{Gott ist gegenw\IeC {\"a}rtig}{59}{subsection*.174}
\contentsline {subsection}{Graduale - Christus factus est}{60}{subsection*.175}
\contentsline {subsection}{Graduale - Locus iste}{60}{subsection*.176}
\contentsline {subsection}{Graduale - Os justi}{61}{subsection*.177}
\contentsline {subsection}{Gro\IeC {\ss }e Messe in c-Moll}{61}{subsection*.178}
\contentsline {subsection}{Hear my prayer, O Lord}{62}{subsection*.179}
\contentsline {subsection}{Hear my prayer, O Lord}{62}{subsection*.180}
\contentsline {subsection}{Hymnus: Ave maris stella}{63}{subsection*.181}
\contentsline {subsection}{H\IeC {\"o}rt, der Engel helle Lieder}{63}{subsection*.182}
\contentsline {subsection}{H\IeC {\o }r, himlens gledessang}{64}{subsection*.183}
\contentsline {subsection}{I Will Praise Thee, O Lord}{64}{subsection*.184}
\contentsline {subsection}{I. Selig sind, die da Leid tragen}{65}{subsection*.185}
\contentsline {subsection}{II. Denn alles Fleisch ist wie Gras}{65}{subsection*.186}
\contentsline {subsection}{III. Herr, lehre doch mich}{66}{subsection*.187}
\contentsline {subsection}{IV. Wie lieblich sind deine Wohnungen}{66}{subsection*.188}
\contentsline {subsection}{Ich bin das Brot des Lebens}{67}{subsection*.189}
\contentsline {subsection}{Ich lasse dich nicht}{67}{subsection*.190}
\contentsline {subsection}{Ich singe dir mit Herz und Mund}{68}{subsection*.191}
\contentsline {subsection}{Ich singe dir mit Herz und Mund}{68}{subsection*.192}
\contentsline {subsection}{Ich steh an deiner Krippen hier}{69}{subsection*.193}
\contentsline {subsection}{Immortal Bach}{69}{subsection*.194}
\contentsline {subsection}{In dulci Jubilo}{70}{subsection*.195}
\contentsline {subsection}{In pride of May}{70}{subsection*.196}
\contentsline {subsection}{Introitus f\IeC {\"u}r die Adventssonntage}{71}{subsection*.197}
\contentsline {subsection}{It is my well-beloved's voice}{71}{subsection*.198}
\contentsline {subsection}{Jauchzet dem Herrn alle Welt}{72}{subsection*.199}
\contentsline {subsection}{Jesu, meine Freude}{72}{subsection*.200}
\contentsline {subsection}{Jesu, meines Lebens Leben}{73}{subsection*.201}
\contentsline {subsection}{Jesus, unser Trost und Leben}{73}{subsection*.202}
\contentsline {subsection}{Johannespassion}{74}{subsection*.203}
\contentsline {subsection}{Joseph, lieber Joseph mein}{74}{subsection*.204}
\contentsline {subsection}{Jubilate Deo}{75}{subsection*.205}
\contentsline {subsection}{Jubilate Deo}{75}{subsection*.206}
\contentsline {subsection}{Komm, o Tod, du Schlafes Bruder}{76}{subsection*.207}
\contentsline {subsection}{Kommet, ihr Hirten}{76}{subsection*.208}
\contentsline {subsection}{Kyrie}{77}{subsection*.209}
\contentsline {subsection}{Kyrie}{77}{subsection*.210}
\contentsline {subsection}{Kyrie}{78}{subsection*.211}
\contentsline {subsection}{Kyrie}{78}{subsection*.212}
\contentsline {subsection}{Kyrie eleison}{79}{subsection*.213}
\contentsline {subsection}{K\IeC {\"o}nige vom Morgenland}{79}{subsection*.214}
\contentsline {subsection}{Laetatus sum}{80}{subsection*.215}
\contentsline {subsection}{Lauda Jerusalemn}{80}{subsection*.216}
\contentsline {subsection}{Laudate}{81}{subsection*.217}
\contentsline {subsection}{Laudate Pueri Dominum}{81}{subsection*.218}
\contentsline {subsection}{Lobet den Herren, alle Heiden}{82}{subsection*.219}
\contentsline {subsection}{Machet die Tore weit}{82}{subsection*.220}
\contentsline {subsection}{Macht hoch die T\IeC {\"u}r}{83}{subsection*.221}
\contentsline {subsection}{Magnificat}{83}{subsection*.222}
\contentsline {subsection}{Magnificat}{84}{subsection*.223}
\contentsline {subsection}{Maria durch ein Dornwald ging}{84}{subsection*.224}
\contentsline {subsection}{Maria durch ein Dornwald ging}{85}{subsection*.225}
\contentsline {subsection}{Missa da Requiem}{85}{subsection*.226}
\contentsline {subsection}{Mitten wir im Leben sind}{86}{subsection*.227}
\contentsline {subsection}{Morgengesang}{86}{subsection*.228}
\contentsline {subsection}{Nachtlied}{87}{subsection*.229}
\contentsline {subsection}{Nisi Dominus}{87}{subsection*.230}
\contentsline {subsection}{Notre P\IeC {\`e}re}{88}{subsection*.231}
\contentsline {subsection}{Nun geh uns auf, du Morgenstern}{88}{subsection*.232}
\contentsline {subsection}{Nun komm, der Heiden Heiland}{89}{subsection*.233}
\contentsline {subsection}{Nun ruhen alle W\IeC {\"a}lder}{89}{subsection*.234}
\contentsline {subsection}{Nunc dimittis}{90}{subsection*.235}
\contentsline {subsection}{O Haupt voll Blut und Wunden}{90}{subsection*.236}
\contentsline {subsection}{O Lord, in thee is all my trust}{91}{subsection*.237}
\contentsline {subsection}{O magnum mysterium}{91}{subsection*.238}
\contentsline {subsection}{O sacrum convivium!}{92}{subsection*.239}
\contentsline {subsection}{Pater Noster}{92}{subsection*.240}
\contentsline {subsection}{Peace I Leave With You}{93}{subsection*.241}
\contentsline {subsection}{Requiem}{93}{subsection*.242}
\contentsline {subsection}{Sactus e Benedictus}{94}{subsection*.243}
\contentsline {subsection}{Salvum fac regem, Domine}{94}{subsection*.244}
\contentsline {subsection}{Schaffe in mir, Gott, ein rein Herz}{95}{subsection*.245}
\contentsline {subsection}{Schau hin nach Golgatha}{95}{subsection*.246}
\contentsline {subsection}{Schnitter Tod}{96}{subsection*.247}
\contentsline {subsection}{Sommarpsalm}{96}{subsection*.248}
\contentsline {subsection}{Sonata sopra \glqq Sancta Maria\grqq }{97}{subsection*.249}
\contentsline {subsection}{Sonne der Gerechtigkeit}{97}{subsection*.250}
\contentsline {subsection}{Sonne der Gerechtigkeit}{98}{subsection*.251}
\contentsline {subsection}{Stille Nacht, heilige Nacht}{98}{subsection*.252}
\contentsline {subsection}{Stille Nacht, heilige Nacht}{99}{subsection*.253}
\contentsline {subsection}{Sumer is icumen in}{99}{subsection*.254}
\contentsline {subsection}{Terra tremuit}{100}{subsection*.255}
\contentsline {subsection}{The first Nowell}{100}{subsection*.256}
\contentsline {subsection}{Thus saith the Lord}{101}{subsection*.257}
\contentsline {subsection}{Tochter Zion, freue dich}{101}{subsection*.258}
\contentsline {subsection}{Tollite Hostias}{102}{subsection*.259}
\contentsline {subsection}{Tu es Petrus}{102}{subsection*.260}
\contentsline {subsection}{Tut mir auf die sch\IeC {\"o}ne Pforte}{103}{subsection*.261}
\contentsline {subsection}{Ubi Caritas}{103}{subsection*.262}
\contentsline {subsection}{Ubi caritas}{104}{subsection*.263}
\contentsline {subsection}{Ubi sunt gaudia}{104}{subsection*.264}
\contentsline {subsection}{Unser lieben Frauen Traum}{105}{subsection*.265}
\contentsline {subsection}{Unsere V\IeC {\"a}ter hofften auf dich}{105}{subsection*.266}
\contentsline {subsection}{V'amo di core}{106}{subsection*.267}
\contentsline {subsection}{V. Ihr habt nun Traurigkeit}{106}{subsection*.268}
\contentsline {subsection}{VI. Denn wir haben hie keine bleibende Statt}{107}{subsection*.269}
\contentsline {subsection}{VII. Selig sind die Toten}{107}{subsection*.270}
\contentsline {subsection}{Verleih uns Frieden gn\IeC {\"a}diglich}{108}{subsection*.271}
\contentsline {subsection}{Verleih uns Frieden gn\IeC {\"a}diglich}{108}{subsection*.272}
\contentsline {subsection}{Vom Himmel hoch, da komm ich her}{109}{subsection*.273}
\contentsline {subsection}{Vom Himmel in die tiefsten Kl\IeC {\"u}fte}{109}{subsection*.274}
\contentsline {subsection}{Von 55 Engeln beh\IeC {\"u}tet}{110}{subsection*.275}
\contentsline {subsection}{Weihnachten}{110}{subsection*.276}
\contentsline {subsection}{Weihnachtsoratorium Teil I: Jauchzet, frohlocket}{111}{subsection*.277}
\contentsline {subsection}{Weihnachtsoratorium Teil II: Und es waren Hirten}{111}{subsection*.278}
\contentsline {subsection}{Weihnachtsoratorium Teil III: Herrscher des Himmels}{112}{subsection*.279}
\contentsline {subsection}{Weihnachtsoratorium Teil IV: Fallt mit Danken, fallt mit Loben}{112}{subsection*.280}
\contentsline {subsection}{Weihnachtsoratorium Teil V: Ehre sei dir, Gott, gesungen}{113}{subsection*.281}
\contentsline {subsection}{Weihnachtsoratorium Teil VI: Herr, wenn die stolzen Feinde schnauben}{113}{subsection*.282}
\contentsline {subsection}{Wer unter dem Schirm des H\IeC {\"o}chsten sitzt}{114}{subsection*.283}
\contentsline {subsection}{Wie liegt die Stadt so w\IeC {\"u}st}{114}{subsection*.284}
\contentsline {subsection}{Wie sch\IeC {\"o}n singt uns der Engel Schar}{115}{subsection*.285}
\contentsline {subsection}{Wie soll ich dich empfangen}{115}{subsection*.286}
\contentsline {subsection}{Wirf dein Anliegen auf den Herrn}{116}{subsection*.287}
\contentsline {subsection}{Wo Engelsstimm im Lied erschallt}{116}{subsection*.288}
\contentsline {subsection}{Zu Bethlem \IeC {\"u}berm Stall}{117}{subsection*.289}
\contentsline {subsection}{h-Moll Messe}{117}{subsection*.290}
\contentsline {section}{Komponisten}{118}{section*.291}
\contentsline {subsection}{Waldemar \IeC {\r A}hl\IeC {\'e}n}{118}{subsection*.292}
\contentsline {subsection}{Johann Christian Bach}{118}{subsection*.293}
\contentsline {subsection}{Johann Sebastian Bach}{118}{subsection*.294}
\contentsline {subsection}{Max Baumann}{119}{subsection*.295}
\contentsline {subsection}{Jacques Berthier}{120}{subsection*.296}
\contentsline {subsection}{Leonard Bernstein}{120}{subsection*.297}
\contentsline {subsection}{Franz Biebl}{120}{subsection*.298}
\contentsline {subsection}{Erhard Bodenschatz}{121}{subsection*.299}
\contentsline {subsection}{Johannes Brahms}{121}{subsection*.300}
\contentsline {subsection}{Benjamin Britten}{122}{subsection*.301}
\contentsline {subsection}{Anton Bruckner}{122}{subsection*.302}
\contentsline {subsection}{Wolfram Buchenberg}{122}{subsection*.303}
\contentsline {subsection}{William Byrd}{123}{subsection*.304}
\contentsline {subsection}{Peter Cornelius}{123}{subsection*.305}
\contentsline {subsection}{Hugo Distler}{123}{subsection*.306}
\contentsline {subsection}{Maurice Durufl\IeC {\'e}}{124}{subsection*.307}
\contentsline {subsection}{Wolfgang Fortner}{124}{subsection*.308}
\contentsline {subsection}{Cornelius Freundt}{124}{subsection*.309}
\contentsline {subsection}{Giovanni Gabrieli}{125}{subsection*.310}
\contentsline {subsection}{Ola Gjeilo}{125}{subsection*.311}
\contentsline {subsection}{Clytus Gottwald}{125}{subsection*.312}
\contentsline {subsection}{Edvard Grieg}{126}{subsection*.313}
\contentsline {subsection}{Lodovico Grossi da Viadana}{126}{subsection*.314}
\contentsline {subsection}{Michael Gusenbauer}{126}{subsection*.315}
\contentsline {subsection}{Andreas Hammerschmidt}{127}{subsection*.316}
\contentsline {subsection}{Georg Friedrich H\IeC {\"a}ndel}{127}{subsection*.317}
\contentsline {subsection}{Hans Leo Ha\IeC {\ss }ler}{127}{subsection*.318}
\contentsline {subsection}{Moritz Hauptmann}{128}{subsection*.319}
\contentsline {subsection}{Philip Hayes}{128}{subsection*.320}
\contentsline {subsection}{Johann Hildebrand}{128}{subsection*.321}
\contentsline {subsection}{Gustav Holst}{129}{subsection*.322}
\contentsline {subsection}{Gottfried August Homilius}{129}{subsection*.323}
\contentsline {subsection}{Egil Hovland}{129}{subsection*.324}
\contentsline {subsection}{John H\IeC {\o }ybye}{130}{subsection*.325}
\contentsline {subsection}{Engelbert Humperdinck}{130}{subsection*.326}
\contentsline {subsection}{Heinrich Kaminski}{130}{subsection*.327}
\contentsline {subsection}{Heike Kiefner-Jesatko}{131}{subsection*.328}
\contentsline {subsection}{Orlando di Lasso}{131}{subsection*.329}
\contentsline {subsection}{Eusebius Mandyczewski}{131}{subsection*.330}
\contentsline {subsection}{Frank Martin}{132}{subsection*.331}
\contentsline {subsection}{Rudolf Mauersberger}{132}{subsection*.332}
\contentsline {subsection}{Felix Mendelssohn Bartholdy}{132}{subsection*.333}
\contentsline {subsection}{Olivier Messiaen}{133}{subsection*.334}
\contentsline {subsection}{Johannes Matthias Michel}{133}{subsection*.335}
\contentsline {subsection}{Josef Michel}{133}{subsection*.336}
\contentsline {subsection}{Vytautas Mi\IeC {\v s}kinis}{134}{subsection*.337}
\contentsline {subsection}{Claudio Monteverdi}{134}{subsection*.338}
\contentsline {subsection}{Wolfgang Amadeus Mozart}{135}{subsection*.339}
\contentsline {subsection}{Fay Neary}{135}{subsection*.340}
\contentsline {subsection}{Joachim Neander}{135}{subsection*.341}
\contentsline {subsection}{Georg Neumark}{136}{subsection*.342}
\contentsline {subsection}{Knut Nystedt}{136}{subsection*.343}
\contentsline {subsection}{Giuseppe Ottavio Pitoni}{136}{subsection*.344}
\contentsline {subsection}{Michael Praetorius}{137}{subsection*.345}
\contentsline {subsection}{Giacomo Puccini}{137}{subsection*.346}
\contentsline {subsection}{Henry Purcell}{138}{subsection*.347}
\contentsline {subsection}{G\IeC {\"u}nter Raphael}{138}{subsection*.348}
\contentsline {subsection}{Max Reger}{138}{subsection*.349}
\contentsline {subsection}{Josef Gabriel Rheinberger}{139}{subsection*.350}
\contentsline {subsection}{Ernst Friedrich Richter}{139}{subsection*.351}
\contentsline {subsection}{Carl Riedel}{139}{subsection*.352}
\contentsline {subsection}{Camille Saint-Sa\IeC {\"e}ns}{140}{subsection*.353}
\contentsline {subsection}{Jan Sandstr\IeC {\"o}m}{140}{subsection*.354}
\contentsline {subsection}{Sven-David Sandstr\IeC {\"o}m}{140}{subsection*.355}
\contentsline {subsection}{Adolf Seifert}{141}{subsection*.356}
\contentsline {subsection}{Thomas Selle}{141}{subsection*.357}
\contentsline {subsection}{Friedrich Silcher}{141}{subsection*.358}
\contentsline {subsection}{Martin Gotthard Schneider}{142}{subsection*.359}
\contentsline {subsection}{Werner Schrade}{142}{subsection*.360}
\contentsline {subsection}{John Stainer}{142}{subsection*.361}
\contentsline {subsection}{Heinrich Sch\IeC {\"u}tz}{143}{subsection*.362}
\contentsline {subsection}{Thomas Tallis}{143}{subsection*.363}
\contentsline {subsection}{Thomas Tomkins}{143}{subsection*.364}
\contentsline {subsection}{Pjotr Iljitsch Tschaikowski}{144}{subsection*.365}
\contentsline {subsection}{Johann Walter}{144}{subsection*.366}
\contentsline {subsection}{Thomas Weelkes}{144}{subsection*.367}
\contentsline {subsection}{Johannes Weyrauch}{145}{subsection*.368}
\contentsline {subsection}{David Willcocks}{145}{subsection*.369}
\contentsline {subsection}{Friedrich Zipp}{145}{subsection*.370}
\contentsline {section}{Sprachen}{146}{section*.371}
\contentsline {subsection}{Althochdeutsch}{146}{subsection*.372}
\contentsline {subsection}{Deutsch}{146}{subsection*.373}
\contentsline {subsection}{Englisch}{149}{subsection*.374}
\contentsline {subsection}{Mittelenglisch}{149}{subsection*.375}
\contentsline {subsection}{Franz\IeC {\"o}sisch}{149}{subsection*.376}
\contentsline {subsection}{Griechisch}{150}{subsection*.377}
\contentsline {subsection}{Hebr\IeC {\"a}isch}{150}{subsection*.378}
\contentsline {subsection}{Italienisch}{150}{subsection*.379}
\contentsline {subsection}{Lateinisch}{150}{subsection*.380}
\contentsline {subsection}{Norwegisch}{151}{subsection*.381}
\contentsline {subsection}{Schwedisch}{152}{subsection*.382}
\contentsline {section}{Schlagworte}{153}{section*.383}
\contentsline {subsection}{Abendmahl}{153}{subsection*.384}
\contentsline {subsection}{Advent}{153}{subsection*.385}
\contentsline {subsection}{Bibel}{153}{subsection*.386}
\contentsline {subsection}{Bibelvers}{155}{subsection*.387}
\contentsline {subsection}{Davidpsalm}{155}{subsection*.388}
\contentsline {subsection}{EKG}{156}{subsection*.389}
\contentsline {subsection}{Engel}{156}{subsection*.390}
\contentsline {subsection}{Epiphanias}{156}{subsection*.391}
\contentsline {subsection}{Karfreitag}{156}{subsection*.392}
\contentsline {subsection}{Maria}{156}{subsection*.393}
\contentsline {subsection}{Paul Gerhardt}{157}{subsection*.394}
\contentsline {subsection}{Psalm}{157}{subsection*.395}
\contentsline {subsection}{Reformationstag}{158}{subsection*.396}
\contentsline {subsection}{Weihnacht}{159}{subsection*.397}
\contentsline {subsection}{weltlich}{159}{subsection*.398}
