import re
import csv

SOURCE = 'RepertoireDB.org'

rx_section = re.compile(r'''
    ^
    \*\s+(?P<title>.+?)\s+:parse:\s*[\r\n]
    (?P<content>[\s\S]+?)
    (?=^\*\s|\Z)
''', re.MULTILINE | re.VERBOSE)

rx_title = re.compile(r'''
    ^
    \*{2}\s+(?P<status>[A-Z_]+)?\s?
    (?P<composer>.*?)\s+--\s+(?P<title>.+?)
    \s*[\r\n]
    (\s*CLOSED: .*?[\r\n])?
    \s*:PROPERTIES:\s*[\r\n]
    \s*(?P<properties>[\s\S]+?)
    \s*:END:
''', re.MULTILINE | re.VERBOSE)

rx_propline = re.compile(r'''
    ^
    \s*
    (:\w+:.*)\s*
    $
''', re.MULTILINE | re.VERBOSE)

rx_property = re.compile(r'''
    :(?P<key>\w+):(?:\s+(?P<value>.*))?
''', re.VERBOSE)


csvcols = (
    'ID',
    'Composer', 
    'ComposerID', 
    'Title', 
    'Collection', 
    'Opus',
    'Year',
    'Published', 
    'Language', 
    'Key', 
    'Voices', 
    'Voicing',
    'Instruments',
    'Soloists',
    'Genre', 
    'Epoch', 
    'Duration',
    'EMTC',                     # expected magnitude of teaching challenge
    'Dedication', 
    'TextSource',
    'Performed', 
    'Score',
    'Wikipedia',
    'Wikidata', 
    'CPDL', 
    'IMSLP', 
    'Youtube',
    'OriginalComposer', 
    'OriginalComposerID', 
    'Remark',
    'Attachments'
    )

def get_titles(section):
    for title in rx_title.finditer( section ):
        lines = rx_propline.findall(title.group('properties'))
        props = {match.group('key') :  match.group('value')
                 for match in (rx_property.match(line)
                               for line in lines)
        }
        yield props

def write_titles_csv():
    with open('Repertoire_Titles.csv', 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=csvcols)
        writer.writeheader()
        for props in get_titles(titlesect):
            csvprops = {k: '' if v is None else v
                        for k, v in props.items()
            }
            writer.writerow(csvprops)

def composers_list():
    for props in get_titles(titlesect):
        try:
            print('| {Composer} | {ComposerID} |'.format(**props))
        except:
            print('| {Composer} | |'.format(**props))



with open(SOURCE, "r") as source:
    content = source.read()
    sections = {section.group('title') : section.group('content')
                for section in rx_section.finditer(content)
    }

    for section, scontent in sections.items():
        if re.match('Titles\s\[\d+/\d+\]', section):
            titlesect = scontent
            break


count = 0                
for prop in get_titles( titlesect ):
    count = count + 1
    print('{Composer} -- {Title}'.format(**prop))
print('{} titles found'.format(count))


write_titles_csv()
