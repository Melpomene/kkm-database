import requests
import re

url = 'https://query.wikidata.org/sparql'
query = '''
SELECT ?itemLabel ?item ?dob ?pobLabel ?dod ?podLabel WHERE {
  # P19:  place of birth
  # P20:  place of death
  # P106: occupation
  # P569: date of birth
  # P570: date of death
  # Q1741: Vienna
  # Q36834: composer
  ?item wdt:P106 wd:Q36834.
  # ?item wdt:P19 wd:Q1741.
  OPTIONAL { ?item wdt:P569 ?dob. }
  OPTIONAL { ?item wdt:P19  ?pob. }
  OPTIONAL { ?item wdt:P570 ?dod. }
  OPTIONAL { ?item wdt:P20  ?pod. }

  
  SERVICE wikibase:label { 
    bd:serviceParam wikibase:language "[AUTO_LANGUAGE],de,en". 
  }
}'''

print('executing query ...')
r = requests.get(url, params = {'format': 'json', 'query': query})

data = r.json()
list = data['results']['bindings']

print(len(list))

for composer in list:
    uri = composer['item']['value']
    entity = re.search('entity/(Q\d+)', uri).group(1)
    name   = composer['itemLabel']['value']
    dob    = composer['dob']['value']
    pob    = composer['pobLabel']['value']
    dod    = composer['dod']['value'] if 'dod' in composer else ''
    pod    = composer['podLabel']['value'] if 'pod' in composer else ''
    print('{:<10} {:<10} {:<10} {:<10} {:<10}'.format(name, dob, pob, dod, pod))
