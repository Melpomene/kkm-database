#!/usr/bin/python3

import requests
import csv                      # for offline database
import datetime as dt

VERSION      = '0.1'
ONLINE       = True

#kkmdb_url    = 'http://kkm-db:8000/kkm.{}.csv.json'
kkmdb_url    = 'http://elaphus.dyndns.org:8042/kkm.{}.csv.json'
wikidata_url = 'https://query.wikidata.org/bigdata/namespace/wdq/sparql'

TITLES       = 1
COMPOSERS    = 2
PERFORMANCES = 3
PROGRAMS     = 4
LOCATIONS    = 5
LANGUAGES    = 6
INDEX        = 7

CQUERY = '''SELECT ?personLabel ?gnd ?gndLabel 
                   ?nat ?natLabel ?natAbbrev
                   ?dob ?pob ?pobLabel 
                   ?dod ?pod ?podLabel 
                   ?wpde ?wpen
WHERE {{
  BIND(wd:{Wikidata-Item} AS ?person)
  ?person wdt:P31 wd:Q5.
  ?person wdt:P21 ?gnd.
  OPTIONAL {{ ?person wdt:P569 ?dob. }}
  OPTIONAL {{ ?person wdt:P19  ?pob. }}
  OPTIONAL {{ ?person wdt:P570 ?dod. }}
  OPTIONAL {{ ?person wdt:P20  ?pod. }}
  OPTIONAL {{ ?person wdt:P27  ?nat. 
              OPTIONAL {{ ?nat    wdt:P298 ?natAbbrev. }} }}
  SERVICE wikibase:label {{ bd:serviceParam wikibase:language "de". }}
  OPTIONAL {{
      ?wpen schema:about ?person .
      ?wpen schema:inLanguage "en" .
      FILTER (SUBSTR(str(?wpen), 1, 25) = "https://en.wikipedia.org/")
    }}
  OPTIONAL {{
      ?wpde schema:about ?person .
      ?wpde schema:inLanguage "de" .
      FILTER (SUBSTR(str(?wpde), 1, 25) = "https://de.wikipedia.org/")
    }}
  }}'''

POSTFIX = '''\\endinput\n
     %%% Local Variables:\n
     %%% TeX-master: "Prototype1"\n
     %%% End:'''


def timestamp():
    prefix = "% created by ProcessData.py, "
    ts = dt.datetime.now().isoformat()
    return ''.join((prefix, VERSION, ts))

def composer_get_wikidata(composer):
    r = requests.get(wikidata_url, params={'query': CQUERY.format(**composer),
                                           'format' : 'json'})
    if not r.json()['results']['bindings']:
        return None
    for k in ('dob', 'dod', 'nat'):
        composer[k] = None
    data = r.json()['results']['bindings'].pop()
    for k in data.keys():
        composer[k] = data[k]['value']

    return composer

def get_sheet(n):
    jdata = 0
    if ONLINE:
        r = requests.get(kkmdb_url.format(n))
        r.raise_for_status()
        jdata = r.json()
    else:
        f = open('kkm.{}.csv'.format(n), 'rU')
        reader = csv.DictReader(f)
        jdata = json.dumps([row for row in reader])
    headers = jdata.pop(0)
    return [ dict(zip(headers, record)) for record in jdata ]


def validate_db():
    """ check whether each entry in 'programmes' has a corresponding key in 'titles'
    and each entry in 'titles' has a corresponding key in 'composers'
    """
    pkeys = (p['Werkcode'] for p in get_sheet(PROGRAMS))
    tkeys = {t['Code']: True for t in get_sheet(TITLES)}

    for pkey in pkeys:
        if pkey not in tkeys:
            print("Key {} missing in 'TITLES'".format(pkey))


def get_composers():
    clist = get_sheet(COMPOSERS)
    for cc in clist:
        c = composer_get_wikidata(cc)
        if not c:
            yield cc
        else:
            try:
                dob = dt.datetime.strptime(c['dob'], '%Y-%m-%dT%H:%M:%SZ')
                c['yob'] = dob.year
            except:
                c['yob'] = None
            try:
                dod = dt.datetime.strptime(c['dod'], '%Y-%m-%dT%H:%M:%SZ')
                c['yod'] = dod.year
            except:
                c['yod'] = None
            yield c

            
def write_titlelist_LaTeX():
    '''write title data (composer, title) as LaTeX-command'''
    print('exporting LaTeX commands for titles ...', end='')
    with open('01_titlelist.tex', 'w') as latex:
        latex.write('% LaTeX commands for titles from the KKM repertoire list\n')
        latex.write('{}\n\n'.format(timestamp()))
        for title in get_sheet(TITLES):
            # command name
            latex.write('\\expandafter\\def\\csname title@{Code}\\endcsname'.format(**title))
            # definition
            latex.write('{{{Komponist} -- {Titel}}}\n'.format(**title))
        latex.write('\n')
    print(' done')
    

def write_performances_LaTeX():
    progdict = {}
    for p in get_sheet(PROGRAMS):
        progdict.setdefault(p['Auftrittscode'], []).append(p['Werkcode'])
        
    with open('10_performances.tex', 'w') as latex:
        latex.write('% performance dates and programmes for the KKM repertoire list\n')
        latex.write('{}\n\n'.format(timestamp()))
        for gig in get_sheet(PERFORMANCES):
            print('processing {Datum}'.format(**gig))
            latex.write('\\begin{{performance}}{{{Datum}}}{{{Titel}}}\n'.format(**gig))
            if gig['Datum'] in progdict:
                for title in progdict[gig['Datum']]:
                    print(title)
                    latex.write('\\perftitle{{{}}}\n'.format(title))
            latex.write('\\end{performance}\n\n')
        latex.write(POSTFIX)

        

def write_titles_LaTeX():
    plist = get_sheet(PROGRAMS)
    with open('11_titles.tex', 'w') as latex:
        latex.write('% title keys and names for the KKM repertoire list\n\n')
        for t in sorted(get_sheet(TITLES), key=lambda t:t['Titel']):
            print('processing {Code}'.format(**t))
            latex.write('\\begin{opus}\n')
            for key in ('Notenmaterial', 'Wikipedia', 'CPDL', 'IMSLP', 'Youtube'):
                if t[key] is not '':
                    t[key] = '\\url{{{}}}'.format(t[key])
            t['Veroffentlichung'] = t.pop('Veröffentlichung')
            t['CKey'] = t['Code'][:4]
            del(t['Komponist'])
            for item in t.items():
                latex.write('  \\op{}{{{}}}\n'.format(*item))
            for item in plist:
                if item['Werkcode'] == t['Code']:
                    latex.write('  \\opPerformance{{{}}}\n'.format(item['Auftrittscode'][:10]))
            latex.write('\\end{opus}\n\n')
        latex.write(POSTFIX)
        

def write_composers_LaTeX():
    titledict = {}
    for t in get_sheet(TITLES):
        titledict.setdefault(t['Code'][:4], []).append(t)

    with open('12_composers.tex', 'w') as item:
        with open('02_composerlist.tex', 'w') as list:
            list.write('% Composer keys and names for the KKM repertoire list\n')
            item.write('% Composer keys and names for the KKM repertoire list\n\n')

            for c in sorted(get_composers(), key=lambda c:(c['Key'][1:], c['Key'][:1])):
                print('processing {Key} ... '.format(**c), end='')
                if 'yob' not in c:
                    c['yob'] = '??'
                if 'yod' not in c:
                    c['yod'] = '??'
                list.write('\\newcommand{{\\composer@{Key}}}{{{Name} ({yob}--{yod})}}\n'.format(**c))
                item.write('\\begin{composer}\n')
                for key in ('wpde', 'wpen'):
                    if key in c and c[key] is not '':
                        c[key] = '\\url{{{}}}'.format(c[key])
                for i in c.items():
                    item.write('  \\com{}{{{}}}\n'.format(*i))
                for t in sorted(titledict[c['Key']], key=lambda x:x['Jahr']):
                    item.write('  \comTitle{{{Code}}}{{{Jahr}}}{{{Titel}}}\n'.format(**t))
                item.write('\\end{composer}\n\n')
                print('')
            list.write(POSTFIX)
                           

def write_languages_LaTeX():
    langdict = {}
    print('reading title list ... ', end='')
    for title in get_sheet(TITLES):
        langdict.setdefault(title['Sprache'], []).append(title['Code'])
    del(langdict[''])
    with open('13_languages.tex', 'w') as lang:
        print('processing language entries ... ', end='')
        lang.write('% Language of titles in the KKM repertoire list\n')
        # lang.write('\section{Schlagworte}\n')
        lang.write('\\makeatletter\n')
        for (key, titles) in sorted(langdict.items()):
            lang.write('\\subsection{{\\expandafter\\uppercase\\csname Lang@{}\\endcsname}}\n'.format(key))
            lang.write('\\label{{Lang-{}}}\n'.format(key))
            for code in titles:
                lang.write('\\csname title@{}\\endcsname\par\n'.format(code))
            lang.write('\n')
        lang.write('\\makeatother\n')
        lang.write(POSTFIX)
        print('done')


def write_index_LaTeX():
    indexdict = {}
    print('reading index list ... ', end='')
    for title in get_sheet(TITLES):
        for key in title['Index'].split(', '):
            indexdict.setdefault(key, []).append(title['Code'])
    del(indexdict[''])
    with open('19_index.tex', 'w') as index:
        print('processing index entries ... ', end='')
        index.write('% Composer keys and names for the KKM repertoire list\n')
        # index.write('\section{Schlagworte}\n')
        index.write('\\makeatletter\n')
        for (key, titles) in sorted(indexdict.items()):
            index.write('\subsection{{{}}}\n'.format(key))
            for code in titles:
                index.write('\\csname title@{}\\endcsname\par\n'.format(code))
        index.write('\\makeatother\n')
        index.write(POSTFIX)
        print('done')


if __name__ == '__main__':
    write_titles_LaTeX()
    write_index_LaTeX()
